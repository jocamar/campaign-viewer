# Campaign Viewer #

A small program to navigate through a map intended to aid DMs on their DnD campaigns. For now it's very bare-bones, only allowing the user to move the camera around and zoom.
This program was written using the Allegro library and that is needed to compile it.