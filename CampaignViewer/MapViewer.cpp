#include "MapViewer.h"

bool MapViewer::initialize()
{
	//--------------------------------------------------------------
	//               ALLEGRO INITIALIZATIONS
	//--------------------------------------------------------------

	if(al_init_image_addon() && al_install_mouse() && al_install_keyboard() && al_init_primitives_addon()) {

		al_init_font_addon();
		if(!al_init_ttf_addon()) 
			return false;

		timer = al_create_timer(1.0 / FPS);
		event_queue = al_create_event_queue();

		al_register_event_source(event_queue, al_get_display_event_source(display));
		al_register_event_source(event_queue, al_get_timer_event_source(timer));
		al_register_event_source(event_queue, al_get_mouse_event_source());
		al_register_event_source(event_queue, al_get_keyboard_event_source());

		al_start_timer(timer); 
	}
	else 
		return false;

	//------------------------------------------------------------------------
	//                      VIEWER INITIALIZATIONS
	//------------------------------------------------------------------------

	font = al_load_ttf_font("Orbitron_Medium.ttf",)
	done = false;
	render = false;
	mouse_x = 0;
	mouse_y = 0;
	mouse_dx = 0;
	mouse_dy = 0;
	in_main_map = true;
	in_search = false;
	mouse_pressed = false;
	transitioning = false;
	scrolling = false;
	curr_area = 0;
	fade_in = 0;

	if(loadInfo())
		return true;
	return false;
}

void MapViewer::start()
{
	srand(time(NULL));

	if(al_init())  //if allegro fails to initiate does nothing, can be removed
	{
		al_set_new_display_flags(ALLEGRO_OPENGL_3_0 | ALLEGRO_RESIZABLE | ALLEGRO_WINDOWED);
		display = al_create_display(WIDTH, HEIGHT);

		if(display)  
		{
			initialize();
			while(!done)  //game loop, updates and renders the game each cycle
			{
				updateViewer();
				renderViewer();
			}

			al_destroy_display(display);  //thrash cleaning functions for when the game is done
			al_destroy_event_queue(event_queue);
			al_destroy_timer(timer);
		}
	}
}

void MapViewer::updateViewer()
{
	ALLEGRO_EVENT ev;  
	al_wait_for_event(event_queue, &ev);

	//==============================================================================================
	//                                   GAME UPDATE
	//==============================================================================================

	if (ev.type == ALLEGRO_EVENT_TIMER)  //these check which type of interrupt it is by checking the type data member of the event struct
	{
		render = true;  //this boolean is used to make the game only render every 1/60 of a second
		if(mouse_pressed && !transitioning && !scrolling) {
			camX -= mouse_dx;
			camY -= mouse_dy;
		}

		if(in_main_map) {
			int mainMapW = al_get_bitmap_width(mainMap);
			int mainMapH = al_get_bitmap_height(mainMap);
			float scale = (float) main_zoom / min(mainMapW,mainMapH);

			if(camX < al_get_display_width(display) / 2)
				camX = al_get_display_width(display) / 2;
			else if(camX > mainMapW + scale*mainMapW - al_get_display_width(display) / 2)
			{
				camX = mainMapW + scale*mainMapW - al_get_display_width(display) / 2;
			}

			if(camY < al_get_display_height(display) / 2)
				camY = al_get_display_height(display) / 2;
			else if(camY > mainMapH + scale*mainMapH - al_get_display_height(display) / 2)
				camY = mainMapH + scale*mainMapH - al_get_display_height(display) / 2;
		}
		else
		{
			int areaMapW = al_get_bitmap_width(interestPoints[curr_area].map);
			int areaMapH = al_get_bitmap_height(interestPoints[curr_area].map);
			float areaScale = (float) interestPoints[curr_area].zoom_thres / min(areaMapH,areaMapW);

			if(interestPoints[curr_area].camX < al_get_display_width(display) / 2)
				interestPoints[curr_area].camX = al_get_display_width(display) / 2;
			else if(interestPoints[curr_area].camX > areaMapW + areaScale*areaMapW - al_get_display_width(display) / 2)
				interestPoints[curr_area].camX = areaMapW + areaScale*areaMapW - al_get_display_width(display) / 2;

			if(interestPoints[curr_area].camY < al_get_display_height(display) / 2)
				interestPoints[curr_area].camY = al_get_display_height(display) / 2;
			else if(interestPoints[curr_area].camY > areaMapH + areaScale*areaMapH - al_get_display_height(display) / 2)
				interestPoints[curr_area].camY = areaMapH + areaScale*areaMapH - al_get_display_height(display) / 2;
		}
	}
	else if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
	{
		done = true;
	} else if(!transitioning && !scrolling) {
		if(ev.type == ALLEGRO_EVENT_MOUSE_AXES)
		{
			mouse_dx = ev.mouse.x - mouse_x;
			mouse_dy = ev.mouse.y - mouse_y;
			mouse_x = ev.mouse.x;
			mouse_y = ev.mouse.y;

			if(mouse_x < 0)
				mouse_x = 0;

			if(mouse_y < 0)
				mouse_y = 0;

			if(mouse_x > al_get_display_width(display))
				mouse_x = al_get_display_width(display);
			if(mouse_y > al_get_display_height(display))
				mouse_y = al_get_display_height(display);

			if(in_main_map) { 
				main_zoom += ev.mouse.dz * 6;
				
				if(main_zoom < main_min_zoom)
					main_zoom = main_min_zoom;
				else if(main_zoom > main_zoom_thres)
					main_zoom = main_zoom_thres;
				else
				{
					int mainMapW = al_get_bitmap_width(mainMap);
					int mainMapH = al_get_bitmap_height(mainMap);
					float offsetX = (float) mainMapW*((float)(ev.mouse.dz*6) / min(mainMapW,mainMapH));
					float offSetY = (float) mainMapH*((float)(ev.mouse.dz*6) / min(mainMapW,mainMapH));
					float scale = (float) (main_zoom-(ev.mouse.dz*6)) / min(mainMapW,mainMapH);

					float posPercentX = camX / (mainMapW * scale + mainMapW);
					float posPercentY = camY / (mainMapH * scale + mainMapH);

					
					camX += offsetX*posPercentX;
					camY += offSetY*posPercentY;
				}


			}
			else 
			{
				interestPoints[curr_area].zoom += ev.mouse.dz * 6;
				if(interestPoints[curr_area].zoom > interestPoints[curr_area].max_zoom)
					interestPoints[curr_area].zoom = interestPoints[curr_area].max_zoom;
				else if(interestPoints[curr_area].zoom > interestPoints[curr_area].zoom_thres)
					interestPoints[curr_area].zoom = interestPoints[curr_area].zoom_thres;
				else
				{
					int areaMapW = al_get_bitmap_width(interestPoints[curr_area].map);
					int areaMapH = al_get_bitmap_height(interestPoints[curr_area].map);
					float offsetX = (float) areaMapW*((float)(ev.mouse.dz*6) / min(areaMapW,areaMapH));
					float offSetY = (float) areaMapH*((float)(ev.mouse.dz*6) / min(areaMapW,areaMapH));
					float areaScale = (float) (main_zoom-(ev.mouse.dz*6)) / min(areaMapH,areaMapW);

					float posPercentX = interestPoints[curr_area].camX / (areaMapW * areaScale + areaMapW);
					float posPercentY = interestPoints[curr_area].camY / (areaMapH * areaScale + areaMapH);

					interestPoints[curr_area].camX += offsetX*posPercentX;
					interestPoints[curr_area].camY += offSetY*posPercentY;
				}
			}
		}
		else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			mouse_pressed = true;
		}
		else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
		{
			mouse_pressed = false;
		}
		else if(ev.type == ALLEGRO_EVENT_KEY_CHAR)
		{
			if(in_search)
			{
				switch(ev.keyboard.keycode)
				{
				case ALLEGRO_KEY_BACKSPACE:
					if(search_query.size() >= 1)
						search_query.pop_back();
					break;
				default:
					// adds a character to the user_name
					if ((ev.keyboard.unichar <= 125) && (ev.keyboard.unichar >= 32) && search_query.size() <= 50)
						search_query.push_back(ev.keyboard.unichar);
					break;
				}
			}
		}
	}
}

void MapViewer::renderViewer()
{
	if(render && al_is_event_queue_empty(event_queue)) //this means it only draws stuff to the screen if there are no events to process, I don't know yet if we'll have to do this or not
	{
		al_clear_to_color(al_map_rgb(0, 0, 0));
		render = false;  //this boolean is used to make the game only render every 1/60 of a second

		if(in_main_map) {
			int mainMapW = al_get_bitmap_width(mainMap);
			int mainMapH = al_get_bitmap_height(mainMap);
			float scale = (float) main_zoom / min(mainMapW,mainMapH);
			al_draw_scaled_bitmap(mainMap,0,0,mainMapW,mainMapH,-(camX-al_get_display_width(display)/2),-(camY-al_get_display_height(display)/2),mainMapW+(mainMapW*scale),mainMapH+(mainMapH*scale),0);

			if(transitioning) {
				int areaMapW = al_get_bitmap_width(interestPoints[curr_area].map);
				int areaMapH = al_get_bitmap_height(interestPoints[curr_area].map);
				float areaScale = (float) interestPoints[curr_area].zoom_thres / min(areaMapH,areaMapW);
				al_draw_tinted_scaled_bitmap(interestPoints[curr_area].map,al_map_rgba_f(1,1,1,fade_in),0,0,areaMapW,areaMapH,-(interestPoints[curr_area].camX-areaMapW/2),-(interestPoints[curr_area].camY-areaMapH/2),areaMapW+(areaMapW*areaScale), areaMapH+(areaMapH*areaScale),0);
			}
		} 
		else
		{
			int areaMapW = al_get_bitmap_width(interestPoints[curr_area].map);
			int areaMapH = al_get_bitmap_height(interestPoints[curr_area].map);
			float areaScale = (float) interestPoints[curr_area].zoom_thres / min(areaMapH,areaMapW);
			al_draw_scaled_bitmap(interestPoints[curr_area].map,0,0,areaMapW,areaMapH,-(interestPoints[curr_area].camX-areaMapW/2),-(interestPoints[curr_area].camY-areaMapH/2),areaMapW+(areaMapW*areaScale), areaMapH+(areaMapH*areaScale),0);

			if(transitioning) {
				int mainMapW = al_get_bitmap_width(mainMap);
				int mainMapH = al_get_bitmap_height(mainMap);
				float scale = (float) main_zoom / min(mainMapW,mainMapH);
				al_draw_tinted_scaled_bitmap(mainMap,al_map_rgba_f(1,1,1,fade_in),0,0,mainMapW,mainMapH,-(camX-mainMapW/2),-(camY-mainMapH/2),mainMapW+(mainMapW*scale),mainMapH+(mainMapH*scale),0);
			}
		}

		al_draw_filled_rectangle(640,10,1270,50,al_map_rgba_f(0,0,0,0.5));
		al_flip_display();
	}
}

bool MapViewer::loadInfo()
{
	pugi::xml_document configDoc;
	pugi::xml_parse_result result = configDoc.load_file(CONFIG_FILE);

	if(result)
	{
		pugi::xml_node mainImage = configDoc.child("map").child("mainImage");


		mainMap = al_load_bitmap(mainImage.attribute("img").value());

		if(mainMap == NULL)
			return false;

		camX = al_get_bitmap_width(mainMap) / 2;
		camY = al_get_bitmap_height(mainMap) / 2;

		if(mainImage.attribute("maxZoom"))
			main_zoom_thres = mainImage.attribute("maxZoom").as_int();
		else
		{
			main_zoom_thres = DEFAULT_MAX_ZOOM;
		}

		if(mainImage.attribute("minZoom"))
			main_min_zoom = mainImage.attribute("minZoom").as_int();
		else
		{
			main_min_zoom = DEFAULT_MIN_ZOOM;
		}
		
		main_zoom = main_min_zoom;
		pugi::xml_node interestPointNode = configDoc.child("map").child("interestPoint");

		while(interestPointNode)
		{
			InterestPoint iPoint;
			iPoint.x = interestPointNode.attribute("x").as_int();
			iPoint.y = interestPointNode.attribute("y").as_int();
			iPoint.w = interestPointNode.attribute("w").as_int();
			iPoint.h = interestPointNode.attribute("h").as_int();
			iPoint.name = interestPointNode.attribute("name").as_string();
			if(interestPointNode.attribute("maxZoom").as_int())
				iPoint.max_zoom = interestPointNode.attribute("maxZoom").as_int();
			else
			{
				iPoint.max_zoom = DEFAULT_MAX_ZOOM;
			}
			iPoint.map = al_load_bitmap(interestPointNode.attribute("img").value());

			if(interestPointNode.attribute("minThres").as_int())
				iPoint.zoom_thres = interestPointNode.attribute("minZoom").as_int();
			else
			{
				iPoint.zoom_thres = DEFAULT_MIN_ZOOM;
			}

			iPoint.map = al_load_bitmap(interestPointNode.attribute("img").value());

			if(iPoint.map == NULL)
				return false;

			iPoint.camX = al_get_bitmap_width(iPoint.map) / 2;
			iPoint.camY = al_get_bitmap_height(iPoint.map) / 2;

			iPoint.zoom = iPoint.zoom_thres;

			interestPoints.push_back(iPoint);
			interestPointNode = interestPointNode.next_sibling("interestPoint");
		}

		return true;

	} else return false;
}

