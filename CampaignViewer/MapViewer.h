#include <allegro5/allegro.h>           //   Includes allegro
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#include <string>
#include <vector>
#include <algorithm>
#include <iostream>

#include "pugixml.hpp"

#define WIDTH 1280
#define HEIGHT 720
#define FPS 60
#define CONFIG_FILE "config.xml"
#define DEFAULT_MAX_ZOOM 500
#define DEFAULT_MIN_ZOOM 100

struct InterestPoint {
	int x, y;
	int w, h;
	ALLEGRO_BITMAP* map;
	std::string name;
	int zoom;
	int max_zoom;
	int zoom_thres;
	int camX, camY;
};

class MapViewer {

	bool done, render;
	int width, height;
	ALLEGRO_DISPLAY *display;
	ALLEGRO_EVENT_QUEUE *event_queue;  
	ALLEGRO_TIMER *timer;

	ALLEGRO_BITMAP* mainMap;
	ALLEGRO_FONT* font;
	std::vector<InterestPoint> interestPoints;

	int mouse_x, mouse_y, mouse_dx, mouse_dy;
	int main_zoom;
	int main_min_zoom; 
	int main_zoom_thres; 
	bool in_main_map, in_search, mouse_pressed;
	bool transitioning, scrolling;
	int curr_area;

	float fade_in;

	int camX, camY;

	std::string search_query;
	bool initialize();
	void updateViewer();
	void renderViewer();
	bool loadInfo();
public:

	
	void start();
};